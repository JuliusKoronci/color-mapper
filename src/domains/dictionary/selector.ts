import { AppState } from '../rootReducer';

export const selectDictionary = (state: AppState) => state.dictionary;

export const selectDictionaryByKey = (state: AppState) => (key: string) => {
  const dictionary = selectDictionary(state);

  const dictionaryKey = Object.keys(dictionary).find(k => k.toLowerCase() === key.toLowerCase())

  return dictionaryKey ? dictionary[dictionaryKey] : key;
}
