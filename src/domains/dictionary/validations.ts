import { curry } from 'ramda';
import { Dictionary } from '../../types/Dictionary';

interface DictionaryItem {
  from: string,
  to: string
};

export const getDictionaryValidationError = curry((dictionary: Dictionary, dictionaryItem: DictionaryItem): string => {
  const keys = Object.keys(dictionary);

  const from = dictionaryItem.from ? dictionaryItem.from.toLowerCase() : '';

  if (keys.map(a => a.toLocaleLowerCase()).includes(from)) {
    return 'The value your are trying to map was already mapped';
  }

  return '';
});
