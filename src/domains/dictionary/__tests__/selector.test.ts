import { selectDictionaryByKey, selectDictionary } from '../selector';

describe('selector', () => {
  it('returns dictionary state', () => {
    expect(selectDictionary({
      product: [],
      dictionary: {
        a: 'b',
      },
    })).toEqual({
      a: 'b',
    });
  });

  it('returns single dictionary by key', () => {
    expect(selectDictionaryByKey({
      product: [],
      dictionary: {
        a: 'b',
      },
    })('A')).toEqual('b');
  });

  it('returns the key if not in the dictionary', () => {
    expect(selectDictionaryByKey({
      product: [],
      dictionary: {
        a: 'b',
      },
    })('c')).toEqual('c');
  });
});
