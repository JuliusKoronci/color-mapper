import { mapDictionaryToView } from '../utils';

describe('mapDictionaryToView', () => {
  it('creates a view ready list', () => {
    expect(mapDictionaryToView({ a: 'b', c: 'd' })).toEqual([
      { from: 'a', to: 'b' },
      { from: 'c', to: 'd' },
    ])
  });
});
