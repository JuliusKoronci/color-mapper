import reducer, {
  addDictionary,
  initialState,
  removeDictionary,
} from '../reducer';

describe('reducer', () => {
  it('returns initial state if not applicable action', () => {
    expect(reducer(undefined, { type: 'ANY' } as any)).toEqual(initialState);
  });

  it('removes a dictionary from state', () => {
    expect(reducer({
      a: 'b',
      b: 'c',
    }, removeDictionary('a') as any)).toEqual({ b: 'c' })
  });

  it('adds a dictionary item to the dictionary', () => {
    expect(reducer({ a: 'b' }, addDictionary({
      from: 'c',
      to: 'd',
    }) as any)).toEqual({
      a: 'b',
      c: 'd',
    })
  });
});
