import { getDictionaryValidationError } from '../validations';

describe('getDictionaryValidationError', () => {
  it('returns an error for already assigned value', () => {
    expect(getDictionaryValidationError({ a: 'b', c: 'd' }, {
      from: 'a',
      to: 'd',
    })).toEqual('The value your are trying to map was already mapped')
  });

  it('returns an empty string for a value which was not yet mapped', () => {
    expect(getDictionaryValidationError({ a: 'b', c: 'd' }, {
      from: 'b',
      to: 'd',
    })).toEqual('')
  });

  it('handles undefined values which may be present because of the form component', () => {
    // @TODO refactor this as it should be handled elsewhere not in validations
    expect(getDictionaryValidationError({ a: 'b', c: 'd' }, {
      from: undefined,
      to: undefined,
    } as any)).toEqual('')
  });
});
