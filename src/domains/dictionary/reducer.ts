import { omit } from 'ramda';
import { createAction, handleActions } from 'redux-actions';
import { Dictionary } from '../../types/Dictionary';

export const initialState: Dictionary = {
  Anthracite: 'Dark Grey',
  'Midnight Black': 'Black Black',
  'Mystic Silver': 'Silver Silver',
};
const REMOVE_DICTIONARY = 'REMOVE_DICTIONARY';

export const removeDictionary = createAction<string>(REMOVE_DICTIONARY);

const ADD_DICTIONARY = 'ADD_DICTIONARY';

export const addDictionary = createAction<{ from: string, to: string }>(ADD_DICTIONARY);

const reducerMap = {
  [REMOVE_DICTIONARY]: (state: Dictionary, action: any) => omit([action.payload], state),
  [ADD_DICTIONARY]: (state: Dictionary, action: any) => ({
    ...state,
    [action.payload.from]: action.payload.to,
  })
};
export default handleActions(reducerMap, initialState)
