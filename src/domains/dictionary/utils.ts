import { Dictionary } from '../../types/Dictionary';

export const mapDictionaryToView = (dictionary: Dictionary) => {
  const keys = Object.keys(dictionary);

  return keys.map(key => ({
    from: key,
    to: dictionary[key],
  }))
};
