import reducer, {
  addProduct,
  removeProduct,
  initialState,
  reducerMap,
} from '../reducer';

describe('reducer', () => {
  it('returns initial state if no valid action', () => {
    expect(reducer(undefined, { type: 'ANY' } as any)).toEqual(initialState);
  });

  it('creates correct action', () => {
    expect(addProduct({
      name: 'test',
      color: 'test',
      price: '5',
    }).payload).toEqual({
      'color': 'test',
      'id': expect.any(String),
      'name': 'test',
      'price': 5,
    })
  });

  it('adds product to state', () => {
    expect(reducerMap.ADD_PRODUCT([], addProduct({
      name: 'test',
      color: 'test',
      price: '5',
    }))).toEqual([
      {
        'color': 'test',
        'id': expect.any(String),
        'name': 'test',
        'price': 5,
      },
    ])
  });

  it('removes product from state', () => {
    expect(reducerMap.REMOVE_PRODUCT([
      {
        'color': 'test',
        'id': '1',
        'name': 'test',
        'price': 5,
      },
    ], removeProduct({
      'color': 'test',
      'id': '1',
      'name': 'test',
      'price': 5,
    }))).toEqual([])
  });
});
