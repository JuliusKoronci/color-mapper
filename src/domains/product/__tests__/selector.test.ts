import { selectProducts } from '../selector';

describe('selectProducts', () => {
  it('returns products from state', () => {
    const product = {
      id: '1',
      color: 'blue',
      name: 'product',
      price: 5,
    };
    expect(selectProducts({
      product: [product],
      dictionary: {},
    })).toEqual([product])
  });
});
