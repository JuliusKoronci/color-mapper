import { AppState } from '../rootReducer';

export const selectProducts = (state: AppState) => state.product;
