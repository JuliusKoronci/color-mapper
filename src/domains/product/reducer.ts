import { handleActions, createAction } from 'redux-actions';
import uniqueid from 'lodash.uniqueid';
import { Product } from '../../types/Product';

type ProductFormValues = {
  price: string;
} & Pick<Product, 'name' | 'color'>

export const initialState: Product[] = [
  {
    id: '1',
    name: 'Apple iPhone 6s',
    color: 'Anthracite',
    price: 769,
  },
  {
    id: '2',
    name: 'Samsung Galaxy S8',
    color: 'Midnight Black',
    price: 569,
  },
  {
    id: '3',
    name: 'Huawei P9',
    color: 'Mystic Silver',
    price: 272,
  },
];

const createProduct = (formProduct: ProductFormValues): Product => ({
  id: uniqueid('_'),
  ...formProduct,
  price: parseInt(formProduct.price, 10),
});

const ADD_PRODUCT = 'ADD_PRODUCT';
export const addProduct = (formProduct: ProductFormValues) =>
  createAction<Product>(ADD_PRODUCT)(createProduct(formProduct));

const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const removeProduct = createAction<Product>(REMOVE_PRODUCT);

export const reducerMap = {
  [ADD_PRODUCT]: (state: Product[], { payload }: ReturnType<typeof addProduct>) =>
    [...state, payload],
  [REMOVE_PRODUCT]: (state: Product[], { payload }: ReturnType<typeof removeProduct>) =>
    state.filter(product => product.id !== payload.id),
};

export default handleActions(reducerMap, initialState)
