import { combineReducers } from 'redux';
import { Dictionary } from '../types/Dictionary';
import { Product } from '../types/Product';
import dictionaryReducer from './dictionary/reducer';
import productReducer from './product/reducer';

const reducers = {
  dictionary: dictionaryReducer,
  product: productReducer,
};

export type AppState = {
  dictionary: Dictionary;
  product: Product[]
}

export const rootReducer = combineReducers<AppState>(reducers);
