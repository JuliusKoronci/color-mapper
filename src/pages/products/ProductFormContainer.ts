import { connect } from 'react-redux';
import { addProduct } from '../../domains/product/reducer';
import { WrappedProductForm } from './ProductForm';


export const mapDispatchToProps = {
  handleSubmit: addProduct,
}

export const ProductFormContainer = connect(undefined, mapDispatchToProps)(WrappedProductForm);
