import { addProduct } from '../../../domains/product/reducer';
import { mapDispatchToProps } from '../ProductFormContainer';

describe('ProductFormContainer', () => {
  it('maps correct action to component', () => {
    expect(mapDispatchToProps.handleSubmit).toEqual(addProduct);
  });
});
