import { shallow } from 'enzyme';
import React from 'react';
import { FormView } from '../FormView';

describe('FormView', () => {
  it('render correct structure without errors', () => {
    const wrapper = shallow(<FormView
      onSubmit={jest.fn()}
      nameError={false}
      priceError={false}
      colorError={false}
      getFieldDecorator={jest.fn().mockReturnValue((comp: any) => comp)}
      getFieldsError={jest.fn().mockReturnValue({})}
      />);
    expect(wrapper).toMatchSnapshot();
  });

  it('render correct structure with errors', () => {
    const wrapper = shallow(<FormView
      onSubmit={jest.fn()}
      nameError={['error']}
      priceError={['error']}
      colorError={['error']}
      getFieldDecorator={jest.fn().mockReturnValue((comp: any) => comp)}
      getFieldsError={jest.fn().mockReturnValue({})}
      />);
    expect(wrapper).toMatchSnapshot();
  });
});
