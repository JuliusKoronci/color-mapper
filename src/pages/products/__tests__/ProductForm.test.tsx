import { shallow } from 'enzyme';
import React from 'react';
import { FormView } from '../FormView';
import { WrappedProductForm, ProductForm } from '../ProductForm';

describe('ProductForm', () => {
  it('ProductForm renders a FormView', () => {
    const wrapper = shallow(<ProductForm
      handleSubmit={jest.fn()} form={
      {
        getFieldDecorator: jest.fn(),
        getFieldsError: jest.fn(),
        getFieldError: jest.fn(),
        isFieldTouched: jest.fn(),
        validateFields: jest.fn(),
      } as any
    }
                               />);
    expect(wrapper.find(FormView)).toHaveLength(1)
  });

  it('ProductForm renders a FormView with error', () => {
    const wrapper = shallow(<ProductForm
      handleSubmit={jest.fn()} form={
      {
        getFieldDecorator: jest.fn(),
        getFieldsError: jest.fn(),
        getFieldError: jest.fn().mockReturnValue('error'),
        isFieldTouched: jest.fn().mockReturnValue(true),
        validateFields: jest.fn(),
      } as any
    }
                               />);
    expect(wrapper.find(FormView).prop('nameError')).toEqual('error');
    expect(wrapper.find(FormView).prop('colorError')).toEqual('error');
    expect(wrapper.find(FormView).prop('priceError')).toEqual('error');
  });

  it('renders a ProductForm', () => {
    const wrapper = shallow(<WrappedProductForm />);
    expect(wrapper.find(ProductForm)).toHaveLength(1);
  });
});
