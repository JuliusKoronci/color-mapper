import { shallow } from 'enzyme';
import React from 'react';
import { ProductsPage, getColumnConfig } from '../ProductsPage';

const wrapper = shallow(
  <ProductsPage
    products={[]}
    selectDictionaryByKey={jest.fn()}
    removeProduct={jest.fn()}
    />
);

describe('ProductsPage', () => {
  it('creates the correct column config', () => {
    expect(getColumnConfig(jest.fn(), jest.fn())).toMatchSnapshot();
  });

  it('maps color trough mapper', () => {
    const mapper = jest.fn();
    const remove = jest.fn();
    const config = getColumnConfig(mapper, remove);

    // @ts-ignore
    config[1].render('a');

    expect(mapper).toHaveBeenCalledWith('a');
  });

  it('returns correct price', () => {
    const config = getColumnConfig(jest.fn(), jest.fn());

    // @ts-ignore
    const price = config[2].render(5);

    expect(price).toEqual('5 CHF');
  });

  it('creates a delete button', () => {
    const config = getColumnConfig(jest.fn(), jest.fn());

    // @ts-ignore
    const button = config[3].render([]);

    expect(button).toMatchInlineSnapshot(`
      <Button
        block={false}
        ghost={false}
        htmlType="button"
        loading={false}
        onClick={[Function]}
      >
        Delete
      </Button>
    `);
  });

  it('doesnt change without confirming', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
