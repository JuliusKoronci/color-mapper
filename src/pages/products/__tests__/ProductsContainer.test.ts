import { removeProduct } from '../../../domains/product/reducer';
import {mapStateToPros, mapDispatchToProps} from '../ProductsContainer';

describe('ProductsContainer', () => {
  it('maps correct state to props', () => {
    expect(mapStateToPros({
      product: [],
      dictionary: {}
    }).products).toEqual([]);
    expect(mapStateToPros({
      product: [],
      dictionary: {}
    }).selectDictionaryByKey).toEqual(expect.any(Function));
  });

  it('maps correct actions to props', () => {
    expect(mapDispatchToProps.removeProduct).toEqual(removeProduct);
  });
});
