import React, { FC } from 'react';
import { Button, Form, Icon, Input, InputNumber } from 'antd';

import { FormComponentProps } from 'antd/lib/form/Form';
import { hasErrors } from '../../utils/hasErrors';

import styles from '../Pages.module.css';

interface OwnProps {
  onSubmit: (e: any) => void;
  nameError: false | undefined | string[];
  priceError: false | undefined | string[];
  colorError: false | undefined | string[];
  getFieldDecorator: FormComponentProps['form']['getFieldDecorator'];
  getFieldsError: FormComponentProps['form']['getFieldsError'];
}

type Props = OwnProps;

export const FormView: FC<Props> = ({ onSubmit, nameError, priceError, getFieldDecorator, colorError, getFieldsError }) =>
  <Form
    layout="inline" onSubmit={onSubmit} className={styles.horizontalForm}>
    <Form.Item
      validateStatus={nameError ? 'error' : ''} help={nameError || ''}>
      {getFieldDecorator('name', {
        rules: [{
          required: true,
          message: 'Please input your products name!',
        }],
      })(
        <Input
          prefix={<Icon type="form" className={styles.inputColor} />}
          placeholder="Name"
          />,
      )}
    </Form.Item>
    <Form.Item
      validateStatus={colorError ? 'error' : ''} help={colorError || ''}>
      {getFieldDecorator('color', {
        rules: [{
          required: true,
          message: 'Please input your products color!',
        }],
      })(
        <Input
          prefix={<Icon
            type="bg-colors" className={styles.inputColor}
                             />}
          placeholder="Color"
          />,
      )}
    </Form.Item>
    <Form.Item
      validateStatus={priceError ? 'error' : ''} help={priceError || ''}>
      {getFieldDecorator('price', {
        rules: [{
          required: true,
          message: 'Please input your products price!',
        }],
      })(
        <InputNumber
          placeholder="Price"
          />,
      )}
    </Form.Item>
    <Form.Item>
      <Button
        type="primary" htmlType="submit"
        disabled={hasErrors(getFieldsError())}>
        Add Product
      </Button>
    </Form.Item>
  </Form>;
