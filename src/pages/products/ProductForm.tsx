import { Form} from 'antd';
import React, { FC } from 'react';

import { FormComponentProps } from 'antd/lib/form/Form';
import { useValidateFormOnMount } from '../../hooks/useValidateFormOnMount';
import { getSubmitHandler } from '../../utils/submitHandler';
import { FormView } from './FormView';

interface OwnProps {
  handleSubmit: (...args: string[]) => void,
}

type Props = OwnProps & FormComponentProps;

export const ProductForm: FC<Props> = ({ handleSubmit, form }) => {
  useValidateFormOnMount(form);

  const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = form;

  const nameError = isFieldTouched('name') && getFieldError('name');
  const colorError = isFieldTouched('color') && getFieldError('color');
  const priceError = isFieldTouched('price') && getFieldError('price');

  const submitHandler = getSubmitHandler(form, handleSubmit);

  return (
    <FormView
      onSubmit={submitHandler}
      colorError={colorError}
      getFieldDecorator={getFieldDecorator}
      nameError={nameError}
      getFieldsError={getFieldsError}
      priceError={priceError}
      />
  );
};

export const WrappedProductForm = Form.create({ name: 'horizontal_product' })(ProductForm);
