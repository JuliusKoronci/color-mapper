import { Button, Table, Typography } from 'antd';
import React, { FC } from 'react';
import { identity } from 'ramda';
import { removeProduct as removeProductAction } from '../../domains/product/reducer';
import { Product } from '../../types/Product';
import { ProductFormContainer } from './ProductFormContainer';

type Mapper = (key: string) => string;

interface OwnProps {
  products: Product[],
  selectDictionaryByKey: Mapper;
  removeProduct: typeof removeProductAction;
}

type Props = OwnProps;

const { Title } = Typography;

export const getColumnConfig = (mapper: Mapper, remove: typeof removeProductAction) => [
  {
    title: 'Product',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Color',
    dataIndex: 'color',
    key: 'age',
    render: (color: string) => mapper(color),
  },
  {
    title: 'Price',
    dataIndex: 'price',
    key: 'address',
    render: (price: number) => `${price} CHF`,
  },
  {
    title: 'Delete',
    key: 'action',
    render: (record: Product) => <Button
      onClick={
        /* istanbul ignore next */
        () => remove(record)}>Delete</Button>,
  },
];

export const ProductsPage: FC<Props> = ({ products, selectDictionaryByKey, removeProduct }) => <>
  <ProductFormContainer />
  <Title level={2}>Original Products</Title>
  <Table
    dataSource={products} columns={getColumnConfig(identity, removeProduct)}
                          />
  <Title level={2}>Transformed Dataset</Title>
  <Table
    dataSource={products}
    columns={getColumnConfig(selectDictionaryByKey, removeProduct)}
    />
</>;
