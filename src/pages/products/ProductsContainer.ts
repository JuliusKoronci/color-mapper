import { connect } from 'react-redux';
import { removeProduct } from '../../domains/product/reducer';
import { AppState } from '../../domains/rootReducer';
import {
  selectProducts,
  selectDictionaryByKey,
} from '../../domains/rootSelectors';
import { ProductsPage } from './ProductsPage';

export const mapStateToPros = (state: AppState) => ({
  products: selectProducts(state),
  selectDictionaryByKey: selectDictionaryByKey(state),
});

export const mapDispatchToProps = {removeProduct}

export const ProductsContainer = connect(mapStateToPros, mapDispatchToProps)(ProductsPage);
