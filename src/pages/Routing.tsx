import React, { FC } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { Layout } from '../components';
import { Routes } from '../config/routes';
import { ProductsContainer } from './products/ProductsContainer';
import { DictionaryContainer } from './dictionary/DictionaryContainer';

export const getProductPage = () => <Layout
  selectedPageKey={Routes.HOME}><ProductsContainer /></Layout>;

export const getDictionaryPage = () => <Layout
  selectedPageKey={Routes.MAPPING}><DictionaryContainer /></Layout>;

export const Routing: FC = () => <BrowserRouter>
  <Route path={Routes.HOME} exact component={getProductPage} />
  <Route path={Routes.MAPPING} component={getDictionaryPage} />
</BrowserRouter>;
