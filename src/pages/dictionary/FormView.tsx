import { Alert, Button, Form, Icon, Input } from 'antd';
import React, { FC } from 'react';
import { hasErrors } from '../../utils/hasErrors';

import styles from '../Pages.module.css';

import { FormComponentProps } from 'antd/lib/form';

interface OwnProps {
  onSubmit: (e: any) => void;
  fromError: false | undefined | string[];
  getFieldDecorator: FormComponentProps['form']['getFieldDecorator'];
  toError: false | undefined | string[];
  getFieldsError: FormComponentProps['form']['getFieldsError'];
  validationError: string;
}

type Props = OwnProps;

export const FormView: FC<Props> = ({ onSubmit, fromError, getFieldDecorator, toError, getFieldsError, validationError }) => <>
  <Form
    layout="inline" onSubmit={onSubmit} className={styles.horizontalForm}
  >
    <Form.Item
      validateStatus={fromError ? 'error' : ''} help={fromError || ''}
    >
      {getFieldDecorator('from', {
        rules: [{
          required: true,
          message: 'Map from!',
        }],
      })(
        <Input
          prefix={<Icon
            type="bg-colors" className={styles.inputColor}
          />}
          placeholder="Map From"
        />,
      )}
    </Form.Item>
    <Form.Item
      validateStatus={toError ? 'error' : ''} help={toError || ''}
    >
      {getFieldDecorator('to', {
        rules: [{
          required: true,
          message: 'Map to!',
        }],
      })(
        <Input
          prefix={<Icon
            type="bg-colors" className={styles.inputColor}
          />}
          placeholder="Map to"
        />,
      )}
    </Form.Item>
    <Form.Item>
      <Button
        type="primary" htmlType="submit"
        disabled={hasErrors(getFieldsError()) || !!validationError}
      >
        Add Dictionary
      </Button>
    </Form.Item>
  </Form>
  {!!validationError && <Alert message={validationError} type="error" />}
</>;

