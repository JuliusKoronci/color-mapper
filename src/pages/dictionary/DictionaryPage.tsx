import React, { FC } from 'react';

import { Button, Table, Typography } from 'antd';
import { removeDictionary as removeDictionaryAction } from '../../domains/dictionary/reducer';
import { mapDictionaryToView } from '../../domains/dictionary/utils';
import { DictionaryFormContainer } from './DictionaryFormContainer';

const { Title } = Typography;

interface OwnProps {
  dictionaries: ReturnType<typeof mapDictionaryToView>
  removeDictionary: typeof removeDictionaryAction,
}

type Props = OwnProps;

export const getColumnConfig = (removeDictionary: typeof removeDictionaryAction) => [
  {
    title: 'Mapped From',
    dataIndex: 'from',
    key: 'from',
  },
  {
    title: 'Mapped To',
    dataIndex: 'to',
    key: 'to',
  },
  {
    title: 'Delete',
    key: 'action',
    render: (record: { from: string }) => <Button
      onClick={
        /* istanbul ignore next */
        () => removeDictionary(record.from)}>Delete</Button>,
  },
];

export const DictionaryPage: FC<Props> = ({ dictionaries, removeDictionary }) => <>
  <DictionaryFormContainer />
  <Title level={2}>Dictionary</Title>
  <Table
    dataSource={dictionaries}
    columns={getColumnConfig(removeDictionary)}
    />
</>;
