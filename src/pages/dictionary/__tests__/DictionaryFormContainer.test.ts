import {
  mapDispatchToProps,
  mapStateToProps,
} from '../DictionaryFormContainer';

describe('DictionaryFormContainer', () => {
  it('maps correct state to props', () => {
    expect(mapStateToProps({
      product: [],
      dictionary: { a: 'b' },
    }).getValidationError).toEqual(expect.any(Function));
  });

  it('maps correct dispatch to props', () => {
    expect(mapDispatchToProps.handleSubmit).toEqual(expect.any(Function));
  });
});
