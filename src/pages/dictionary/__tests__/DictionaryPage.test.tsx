import { shallow } from 'enzyme';
import React from 'react';
import { getColumnConfig, DictionaryPage } from '../DictionaryPage';

describe('DictionaryPage', () => {
  it('has correct column config', () => {
    expect(getColumnConfig(jest.fn())).toMatchSnapshot();
  });

  it('adds a delete button', () => {
    const removeDictionary = jest.fn();
    // @ts-ignore
    const config = getColumnConfig(removeDictionary)[2].render({ from: 'a' });

    expect(config).toMatchInlineSnapshot(`
      <Button
        block={false}
        ghost={false}
        htmlType="button"
        loading={false}
        onClick={[Function]}
      >
        Delete
      </Button>
    `);
  });

  it('doesnt change without confirming', () => {
    expect(
      shallow(<DictionaryPage dictionaries={[]} removeDictionary={jest.fn()} />)
    ).toMatchSnapshot();
  });
});
