import React from 'react';
import { shallow } from 'enzyme';
import { WrappedDictionaryForm, DictionaryForm } from '../DictionaryForm';
import { FormView } from '../FormView';


describe('DictionaryForm', () => {
  it('renders a FormView', () => {
    const wrapper = shallow(<DictionaryForm
      handleSubmit={jest.fn()}
      form={
        {
          getFieldDecorator: jest.fn(),
          getFieldsError: jest.fn(),
          getFieldsValue: jest.fn(),
          getFieldError: jest.fn(),
          isFieldTouched: jest.fn(),
          validateFields: jest.fn(),
        } as any
      }
      dictionary={{ a: 'b' }}
      getValidationError={jest.fn()}
      />);

    expect(wrapper.find(FormView)).toHaveLength(1);
  });

  it('renders a FormView with error', () => {
    const wrapper = shallow(<DictionaryForm
      handleSubmit={jest.fn()}
      form={
        {
          getFieldDecorator: jest.fn(),
          getFieldsError: jest.fn(),
          getFieldsValue: jest.fn(),
          getFieldError: jest.fn().mockReturnValue('error'),
          isFieldTouched: jest.fn().mockReturnValue(true),
          validateFields: jest.fn(),
        } as any
      }
      dictionary={{ a: 'b' }}
      getValidationError={jest.fn()}
      />);

    expect(wrapper.find(FormView).prop('fromError')).toEqual('error')
    expect(wrapper.find(FormView).prop('toError')).toEqual('error')
  });

  it('renders a DictionaryForm', () => {
    const wrapper = shallow(<WrappedDictionaryForm />);
    expect(wrapper.find(DictionaryForm)).toHaveLength(1);
  });
});
