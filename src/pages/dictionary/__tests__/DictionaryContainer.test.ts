import { removeDictionary } from '../../../domains/dictionary/reducer';
import { mapDispatchToProps, mapStateToProps } from '../DictionaryContainer';

describe('DictionaryContainer', () => {
  it('maps correct state to props', () => {
    expect(mapStateToProps({
      product: [],
      dictionary: { a: 'b' },
    }).dictionaries).toEqual([{ from: 'a', to: 'b' }])
  });

  it('maps correct actions to props', () => {
    expect(mapDispatchToProps.removeDictionary).toEqual(removeDictionary);
  });
});
