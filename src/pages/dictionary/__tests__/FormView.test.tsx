import { shallow } from 'enzyme';
import React from 'react';
import { FormView } from '../FormView';

describe('FormView', () => {
  it('renders correct structure without errors', () => {
    const wrapper = shallow(<FormView
      onSubmit={jest.fn()}
      fromError={false}
      toError={false}
      validationError=""
      getFieldDecorator={jest.fn().mockReturnValue((comp: any) => comp)}
      getFieldsError={jest.fn().mockReturnValue({})}
      />);

    expect(wrapper).toMatchSnapshot();
  });

  it('renders correct structure with errors', () => {
    const wrapper = shallow(<FormView
      onSubmit={jest.fn()}
      fromError={['error']}
      toError={['error']}
      validationError="Some Error"
      getFieldDecorator={jest.fn().mockReturnValue((comp: any) => comp)}
      getFieldsError={jest.fn().mockReturnValue({})}
      />);

    expect(wrapper).toMatchSnapshot();
  });
});
