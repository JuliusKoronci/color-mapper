import { compose } from 'ramda';
import { connect } from 'react-redux';
import {
  removeDictionary,
} from '../../domains/dictionary/reducer';
import { mapDictionaryToView } from '../../domains/dictionary/utils';
import { AppState } from '../../domains/rootReducer';
import { selectDictionary } from '../../domains/rootSelectors';
import { DictionaryPage } from './DictionaryPage';

export const mapStateToProps = (state: AppState) => ({
  dictionaries: compose(mapDictionaryToView, selectDictionary)(state),
});

export const mapDispatchToProps = {
  removeDictionary
};

export const DictionaryContainer = connect(mapStateToProps, mapDispatchToProps)(DictionaryPage);
