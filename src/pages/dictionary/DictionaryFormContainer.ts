import { connect } from 'react-redux';
import { addDictionary } from '../../domains/dictionary/reducer';
import { getDictionaryValidationError } from '../../domains/dictionary/validations';
import { AppState } from '../../domains/rootReducer';
import { selectDictionary } from '../../domains/rootSelectors';
import { WrappedDictionaryForm } from './DictionaryForm';

export const mapDispatchToProps = {
  handleSubmit: addDictionary,
}

export const mapStateToProps = (state: AppState) => {
  const dictionary = selectDictionary(state);

  return {
    getValidationError: getDictionaryValidationError(dictionary),
  }
};

export const DictionaryFormContainer = connect(mapStateToProps, mapDispatchToProps)(WrappedDictionaryForm);
