import { Form } from 'antd';
import React, { FC } from 'react';

import { FormComponentProps } from 'antd/lib/form/Form';
import { addDictionary } from '../../domains/dictionary/reducer';
import { useValidateFormOnMount } from '../../hooks/useValidateFormOnMount';
import { Dictionary } from '../../types/Dictionary';
import { getSubmitHandler } from '../../utils/submitHandler';
import { FormView } from './FormView';

interface OwnProps {
  handleSubmit: typeof addDictionary,
  dictionary: Dictionary;
  getValidationError: ({ from, to }: { from: string, to: string }) => string
}

type Props = OwnProps & FormComponentProps;

export const DictionaryForm: FC<Props> = ({ handleSubmit, form, getValidationError }) => {

  const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched, getFieldsValue } = form;

  const validationError = getValidationError(getFieldsValue(['from', 'to']) as any);

  useValidateFormOnMount(form);

  const fromError = isFieldTouched('from') && getFieldError('from');
  const toError = isFieldTouched('to') && getFieldError('to');

  const submitHandler = getSubmitHandler(form, handleSubmit);

  return (
    <FormView
      onSubmit={submitHandler}
      fromError={fromError}
      getFieldDecorator={getFieldDecorator}
      toError={toError}
      getFieldsError={getFieldsError}
      validationError={validationError}
      />
  );
};

export const WrappedDictionaryForm = Form.create({ name: 'horizontal_product' })(DictionaryForm);
