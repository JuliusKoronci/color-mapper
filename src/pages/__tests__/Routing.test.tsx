import { shallow } from 'enzyme';
import React from 'react';
import { DictionaryContainer } from '../dictionary/DictionaryContainer';
import { ProductsContainer } from '../products/ProductsContainer';
import { Routing, getDictionaryPage, getProductPage } from '../Routing';

const wrapper = shallow(<Routing />);

describe('Routing', () => {
  it('doesnt change without confirming', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('returns the dictionary page', () => {
    expect(shallow(getDictionaryPage()).find(DictionaryContainer)).toHaveLength(1);
  });

  it('returns the product page', () => {
    expect(shallow(getProductPage()).find(ProductsContainer)).toHaveLength(1);
  });
});
