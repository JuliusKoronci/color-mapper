import 'antd/dist/antd.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { getStore } from './config/createStore';
import { Routing } from './pages/Routing';

ReactDOM.render(<Provider store={getStore()}>
    <Routing />
  </Provider>,
  document.getElementById('root'));
