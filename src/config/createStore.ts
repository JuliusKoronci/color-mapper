import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { identity } from 'ramda';

import { rootReducer } from '../domains/rootReducer';

export const getStore = () => createStore(rootReducer, compose(
  applyMiddleware(thunk),
      /* istanbul ignore next */
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore there is no need to re-declare window for chrome dev tools
  window.devToolsExtension ? window.devToolsExtension() : identity,
));
