import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import * as redux from 'redux';
import { rootReducer } from '../../domains/rootReducer';
import { getStore } from '../createStore';

describe('createStore', () => {
  const createStoreSpy = jest.spyOn(redux, 'createStore');
  const applyMiddlewareSpy = jest.spyOn(redux, 'applyMiddleware');

  getStore();

  it('calls createStore with root reducers', () => {
    expect(createStoreSpy).toHaveBeenCalledWith(rootReducer, expect.any(Function))
  });

  it('calls applyMiddleware with thunks', () => {
    expect(applyMiddlewareSpy).toHaveBeenCalledWith(thunk)
  });
});
