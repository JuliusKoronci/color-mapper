export enum Routes {
  HOME = '/',
  MAPPING = '/dictionary',
  FORM = '/add-dictionary'
}
