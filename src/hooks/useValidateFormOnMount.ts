import { useEffect } from 'react';

export const useValidateFormOnMount = (form: { validateFields: () => void }) => {
  useEffect(() => {
    form.validateFields();
    // eslint-disable-next-line
  }, []);
};
