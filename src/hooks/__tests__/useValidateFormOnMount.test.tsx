import { mount } from 'enzyme';
import React, { FC } from 'react';
import { act } from 'react-dom/test-utils';
import { useValidateFormOnMount } from '../useValidateFormOnMount';

describe('useValidateFormOnMount', () => {
  const form = {
    validateFields: jest.fn(),
  };
  const Comp: FC = () => {
    useValidateFormOnMount(form);

    return <div>test</div>
  }

  it('cals validate fields on mount', () => {
    const wrapper = mount(<Comp />);

    act(() => {wrapper.unmount()});

    expect(form.validateFields).toHaveBeenCalledTimes(1);
  });
});
