import { FormComponentProps } from 'antd/lib/form/Form';

export const getSubmitHandler = <T>(form: FormComponentProps['form'], handleSubmit: (values: T) => void) => (e: any) => {
  e.preventDefault();
  form.validateFields((err, values) => {
    if (!err) {
      handleSubmit(values);
      form.resetFields();
    }
  })
};
