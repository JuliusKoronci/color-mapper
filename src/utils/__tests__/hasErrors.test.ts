import { hasErrors } from '../hasErrors';

describe('hasErrors', () => {
  it('returns true if object has a matching error', () => {
    expect(hasErrors({
      name: '',
      surname: 'error',
    })).toBe(true);
  });

  it('returns false if object has no matching error', () => {
    expect(hasErrors({ name: '', surname: '' })).toBe(false);
  });
});
