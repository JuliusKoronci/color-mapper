import { getSubmitHandler } from '../submitHandler';

describe('submitHandler', () => {
  it('handles success case', () => {
    const submit = jest.fn();
    const reset = jest.fn();

    const handler = getSubmitHandler({
      validateFields: jest.fn().mockImplementation(fn => fn(false, 'test')),
      resetFields: reset,
    } as any, submit);

    handler({ preventDefault: jest.fn() });

    expect(submit).toHaveBeenCalledWith('test');
    expect(reset).toHaveBeenCalledTimes(1);
  });

  it('handles error case', () => {
    const submit = jest.fn();
    const reset = jest.fn();

    const handler = getSubmitHandler({
      validateFields: jest.fn().mockImplementation(fn => fn(true, 'test')),
      resetFields: reset,
    } as any, submit);

    handler({ preventDefault: jest.fn() });

    expect(submit).toHaveBeenCalledTimes(0)
    expect(reset).toHaveBeenCalledTimes(0);
  });
});
