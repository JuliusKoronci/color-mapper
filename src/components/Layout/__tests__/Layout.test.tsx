import React from 'react';
import { shallow } from 'enzyme';
import { Layout as LayoutAntd, Menu } from 'antd';
import { Routes } from '../../../config/routes';
import { Layout } from '../Layout';


const { Content } = LayoutAntd;

const wrapper = shallow(<Layout selectedPageKey={Routes.HOME}><p className="testCase">test</p></Layout>);

describe('Layout', () => {
  it('marks correct route as selected', () => {
    expect(wrapper.find(Menu).prop('defaultSelectedKeys')).toEqual([Routes.HOME])
  });

  it('renders children into content', () => {
    expect(wrapper.find(Content).find('.testCase').text()).toEqual('test')
  });

  it('doesnt change without confirmation', () => {
    expect(wrapper.dive().dive()).toMatchSnapshot();
  });
});
