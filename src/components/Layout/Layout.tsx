import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { Layout as LayoutAntd, Menu, Breadcrumb } from 'antd';
import { Routes } from '../../config/routes';

import styles from './Layout.module.css';

const { Header, Content, Footer } = LayoutAntd;

interface LayoutProps {
  selectedPageKey: Routes;
}

// this would be ideally somewhere else and mapped to translations but I assume
// it is enough for this demo app :)
const PathMap: any = {
  [Routes.HOME]: 'Products',
  [Routes.MAPPING]: 'Dictionary',
};

export const Layout: FC<LayoutProps> = ({ children, selectedPageKey }) =>
  <LayoutAntd className="layout">
    <Header>
      <div className={styles.logo}>GLENCORE</div>
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={[selectedPageKey]}
        className={styles.menu}>
        <Menu.Item key={Routes.HOME}><Link
          to={Routes.HOME}>{PathMap[Routes.HOME]}</Link></Menu.Item>
        <Menu.Item key={Routes.MAPPING}><Link
          to={Routes.MAPPING}>{PathMap[Routes.MAPPING]}</Link></Menu.Item>
      </Menu>
    </Header>
    <Content className={styles.content}>
      <Breadcrumb className={styles.breadCrumbs}>
        <Breadcrumb.Item><Link to={Routes.HOME}>Home</Link></Breadcrumb.Item>
        <Breadcrumb.Item>{PathMap[selectedPageKey]}</Breadcrumb.Item>
      </Breadcrumb>
      <div
        className={styles.children}>{children}</div>
    </Content>
    <Footer className={styles.footer}>Ant Design ©2018 Created by Ant
      UED</Footer>
  </LayoutAntd>;
